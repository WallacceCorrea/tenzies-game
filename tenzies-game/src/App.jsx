import { useState } from "react";
import { nanoid } from "nanoid";
import "./App.css";
import Dice from "./Components/Dice/Dice";
import { useEffect } from "react";
import Confetti from "react-confetti";

function App() {
  const [dice, setDice] = useState(allNewDice());
  const [tenzies, setTenzies] = useState(false);

  useEffect(() => {
    const allHeld = dice.every((dice) => dice.isHeld); // true when all dices are selected
    const firstValue = dice[0].value; // gets the first value from the dice array value
    // console.log(firstValue)
    const allSameValue = dice.every((dice) => dice.value === firstValue);
    // console.log(allSameValue)
    if (allHeld && allSameValue) {
      setTenzies(true);
    }
  }, [dice]);

  function newDice() {
    return {
      id: nanoid(),
      value: Math.ceil(Math.random() * 6),
      isHeld: false,
    };
  }

  function allNewDice() {
    const newDiceArr = [];
    for (let i = 0; i < 10; i++) {
      newDiceArr.push(newDice());
    }
    return newDiceArr;
  }

  const diceElements = dice.map((dice) => (
    <Dice
      diceValue={dice.value}
      key={dice.id}
      isHeld={dice.isHeld}
      handleClick={() => holdDice(dice.id)}
    ></Dice>
  ));

  function rollDice() {
    if (!tenzies) {
      setDice((oldDice) =>
        oldDice.map((dice) => {
          return dice.isHeld ? dice : newDice();
        })
      );
    } else {
      setTenzies(false)
      setDice(allNewDice())
    }
  }

  function holdDice(id) {
    setDice((prev) =>
      prev.map((dice) => {
        return dice.id === id ? { ...dice, isHeld: !dice.isHeld } : dice;
      })
    );
  }

  return (
    <div className="App">
      <main>
        {tenzies && <Confetti />}
        <div className="board-container">
          <div className="text">
            <h1>Tenzies</h1>
            <p>
              Roll until all dice are the same. Click each die to freeze it at
              its current value between rolls.
            </p>
          </div>
          <div className="dice-container">{diceElements}</div>
          <button onClick={rollDice}>{tenzies ? "New Game" : "Roll"}</button>
        </div>
      </main>
    </div>
  );
}

export default App;

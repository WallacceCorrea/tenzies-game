import "./Dice.css";
import "./../../index.css";


export default function Dice(props) {

  const styles = props.isHeld? "dice selected" : "dice" 

  return (
    <div 
      className={styles}
      onClick={props.handleClick}>
      {props.diceValue}
    </div>
  );
}
